# python-firewall
Un firewall de Linux implementado en Python

## Introducción

Esta herramienta no pretende suplantar a iptables por el contraria lo usa de backend, intenta ser un conjunto de reglas minimalista que a he ido depurando y mejorando a lo largo de mi carrera como administrador y que me han solucionado la vida evitando situaciones desagradables donde otros Firewalls comprados fallaban tal vez por falta de experiencia por parte de las personas que los configuron, he visto empresas que han tenido que pagar facturas de 2000 dolares en llamadas internacionales por tener mal configurado su Firewall  un par de miles de dólares, lujo que yo no podía permitirme.

Este software es una herramienta que permite facilmente configurar que partes del firewall se activan o no de acuerdo a las necesidades particulares, es extencible porque permite definir el conjunto de reglas de iptables que se aplican de forma personalizada y finalmente lo más importante es minimalista. La idea es que todos puedan leer fácilmente las reglas de Firewall que se crean y comprender su funcionamiento.

Por esto antes de realizar una critica o proponer un cambio considerar que la primer motivación fue hacer todo el desarrollo de forma simple y minimalista. Para que sea fácil de entender.


## Motivaciones

Un Firewall más flotando en internet. ¿Pero porque otro Firewall habiendo tantos programas que implementan un Firewall disponibles en internet?

Para contestar esto hay que hacer un poco de historia, hace más de 25 años que me dedico a la administración de servidores y en cada instalación tenía que levantar reglas de Firewall, casi siempre era la propia máquina que administraba la que levantaba la conección a Internet. Comprendí al poco tiempo que necesitaba interactuar con el Firewall de Linux, bloquando o permitiendo tráfico hacia Internet, redireccionando puertos o creando enmascaramientos.

Vi varios productos de software que se encargaban de esa tarea, sin embargo lo que hacían por debajo me parecía oscuro.
Pese a lo que digan varias personas iptables, comando principal usado para interactuar con el Firewall de Linux es bastante simple de usar y fácil de leer. Solo es necesario comprender la lógica del funcionamiento que es bastante simple.

Ej.: si quiero bloquar el tráfico tcp que proviene al puerto 22 por la interfaz ppp0, la regla asociada es:
    iptables -I INPUT -i ppp0 -p tcp --dport 22  -j DROP

    Bastante explicita por cierto, ¿porque hacerla más complicada...?

Por supuesto existen reglas más complejas que estas dependiendo lo que se quiera realizar, pero en particular más complejo me a resultado intentar entender el conjunto de reglas que crean otros productos de software que son un frontend del Firewall de iptables y de iproute2 de Linux, creando tablas y paseando el tráfico de una tabla a otra imponiendo una lógica particular que implementaron los desarrolladores de la solución particular. Con esto no estoy diciendo que mi solución sea mejor que las otras, ni que las otras sean malas.
Solo estoy diciendo que yo considero que iptables es simple y mi solución esta cercana a él.

Con el tiempo comense a darme cuenta que en general escribía las mismas reglas para protejer las distintas aplicaciones o puertos y decidí implementar un Firewall en shell script para poder hacerlo más portable, si bien las reglas de iptables quedaban casi idénticas a las reglas ejecutadas en la linea de commandos otras partes del script tenían una sintaxis propias de shell script que no terminaban de gustar. A esta versión la llame la versión 0.5 del Firewall.

Recientemente (año 2019) tuve que actualizar una serie de servidores que tenía funcionando por más de 10 años y decidí a realizar el Firewall de forma más practica y robusta, no solo por las reglas sino por las operaciones que ofrece el Firewall y es así que surgió firewall.py el programa que acompaña este documento.

Comparto entonces más que un Firewall, mi conocimiento transformado en código para que puedan usarlo en su beneficio y para los que recien se inician aprender algunas buenas practicas que me han dado muy buenos resultados.


## Pre requerimentos de softwre

En el desarrollo del software se intento usar la menor cantidad de bibliotecas y que estas estuvieran disponibles por defecto en cualquier instalación de Linux, por este motivo no se incluye un archivo setup.py.

firewall.py requiere python3 instalado y las siguientes bibliotecas:

    * sys
    * os
    * subprocess
    * shlex
    * select

y los siguientes binarios:

    * iptables
    * ipset
    * wget
    * iptables-save
    * iptables-restore


## Como usarlo

Es recomendable ejecturar ./firewall download antes de usar el firewall, este comando creara la carpeta zones y descargara los archivos de zonas de internet (rangos IP asignados a acada país) y también las listas de redes con mala reputación.

Editar el archivo conf.py de acuerdo a las necesidades y luego ejecutar ./firewall start

No es necesario editar firewall.py a menos que se quiera hacer un nuevo release.
En caso de necesitar reglas personalizadas se pueden setear en las variables en conf.py pre_iptables y post_iptables que acepta reglas de iptables separadas por enter se explicara mejor en la sección siguiente.

conf.py es incluido por firewall.py es un archivo con sintaxis valida de python3, si no sabes python  (aprende jeje), no se precisa ser un experto, solo saber que:

nombre_variable = 'hola mundo!' es una variable de tipo string
nombre_variable = 23 es una variable de tipo numero
nombre_variable = ['hola mundo!', 'chau mundo!'] es una variable de tipo lista de strings
nombre_variable = [23, 42] es una variable de tipo lista de números
nombre_variable = True es una variable de tipo booleano con valor True y si dijera False sería falso...
nombre_variable = """

"""  En este caso es un string con varias lineas ( \n )

Y finalmente el caracter # indica el comienzo de un comentario todo lo que este a la derecha del caracter # es ignorado

firewall.py requiere ser ejecutado como root porque llama a iptables e ipset que requieren permiso de administración (root) para crear las reglas.


## Parámetros que acepta firewall.py

El Firewall haceta 1 solo parámetro ./firewall <parametro> y este puede ser:

start : aplica las reglas de Firewall a la maquina donde se ejecuta
stop : elimina todas las reglas de Firewall
restart : Es equivalente a hacer ./firewall stop y luego ./firewall start
test : Almacena las reglas actuales en archivos temporales, luego aplica las reglas de acuerdo a los setos de conf.py, y pregunta si se quieren aplicar  de forma definitiva o no. Si no se contesta "Y" o "y"  en 50 segundo deshace todas las reglas que aplico y restaura las que había salvado previamente. Útil para verificar si se tiene acceso al servidor una vez aplicada las reglas si se contesta "Y" o "y" a la pregunta aplica las reglas de forma permanente.
simulate : No aplica ninguna regla e imprime en pantalla el conjunto de reglas que aplicaría
status : Imprime en pantalla el resultado de ejecutar iptables -L -v -n
status-ipset : Imprime en pantalla el resultado de ejecutar ipset list
download : Descarga en la carpeta zones las listas de IP's y redes asignadas a cada pais y las listas de IP's y redes de los sitios que llevan listas de mala reputación de IP's se muestra el avance de cada descarga.
silent_download : Igual que download  pero si imprimir nada en pantalla
save : Salva las reglas en los archivos: /tmp/ipset_test_rules_firewall y /tmp/iptables_test_rules_firewall
help: Imprime una breve ayuda.

## Variables a setear en conf.py

El archivo de configuración esta dividida en secciones, se explican a continuación las distintas partes:


### Reglas genericas


Interface a la interface de red que se le van a aplicar las reglas
interface = 'eth0'

Reglas personalizdas de iptables que se van a aplicar antes que el programa firewall aplique sus reglas. Los comentarios y lineas vacias serán ignoradas. Por ejemplo:
pre_iptables = """
iptables -I INPUT -i ppp0 -p tcp --dport 22  -j DROP
iptables -I INPUT -i ppp0 -p tcp --dport 23  -j drop
iptables -t nat -A POSTROUTING -o ppp0 -j MASQUERADE
"""

Nota: Observar drop en minuscula y DROP en mayuscula, esto es correcto el DROP en mayuscula es el drop de iptables y el drop en minuscula corresponde a una tabla que el firewall crea para que todos los paquetes que se manden a esa tabla se registran en el archivo de log (en Debian 9 /var/log/kernel.log) y luego se eliminan.


Reglas personalizdas de iptables que se van a aplicar antes que el programa firewall aplique sus reglas. Los comentarios y luneas vacias serán ignoradas.
post_iptables = """
iptables -A FORWARD -i eth2 -s 192.168.2.0/24 -j ACCPET
iptables -A FORWARD -i eth2 -d 192.168.2.0/24 -j ACCPET
"""

Si at_end_add_input_forward_drop = True se agregan reglas que bloquean todo el trafico en la tabla INPUT Y FORWARD estas reglas se agregan debajo de todas las reglas definidas en  post_iptables. Si se setea en False no agrega las reglas de bloqueo.
at_end_add_input_forward_drop = True


Limita la cantidad de paquetes icmp (ping por ejemplo) al valor seteado en icmp_limit en el ejemplo a 10 por segundo la regla que levanta el Firewall permite una rafaga inicial de 10 paquetes icmp antes de aplicar los limites de valocidad permitidos.
icmp_limit = '10/s'

Se indica que se registre cada paquete que se va a eliminar por las reglas que aplica el Firewall
log = True # if true log all blocked traphic

Habilita el control de cantidad de conexiones nuevas que se permiten desde una misma dirección IP con destino a un mismo puerto, para el protocolo tcp y el protocolo udp, permite una rafaga inicial de udp_tcp_burst y luego una velocidad de udp_tcp_rate.
enable_udp_tcp_thresshold  = True  # The filter use source ip and destination port to limit new ucp, tcp connections
udp_tcp_rate = '6/minute'  # speed of udp or tcp new connectins
udp_tcp_burst = '10'  # Burst of new udp  or tcp pertmited


### Reglas genericas para las distintas zonas

Bloquea todo el tráfico proveniente de los paises definidos en zones_deny.
Permite el trafico entrante a los puertos designasdos en tcp_port_zones_accept y udp_port_zones_accept desde los paíes definidos en zones_accept.
El bloqueo se establece usando el prefijo del país para más información luego de ejecutar ./firewall download, se pueden ver los archivos de prefijos disponibles mirando el contenido de la carpeta zones.
Si existe el mismo prefijo en las listas zones_accept y zones_deny se le da más preferencia a zones_deny.

Nota importante: Si no define algun valor in zones_accept todos los puertos definidos en tcp_port_zones_accept y udp_port_zones_accept serán bloqueados.

zones_accept = ['uy']
tcp_port_zones_accept = [22,]
udp_port_zones_accept = []
zones_deny = []


### Reglas especificas para protocolo SIP para telefonía IP

En esta sección se definen los paquetes que voip que se van a permitir o denegar todas las reglas se aplican a los paquetes que prvienen al voip_signal_port protocolo voip_protocol

Zonas de paises donde se permiten llamadas
voip_zones_accept = ['uy']

Si es True coloca las reglas para voip en la tabla input
enable_voip_rules_input = False

Si es True coloca las reglas para voip en la tabla forward, en caso que el sistema de telefonía no se encuentre en el equipo y este este configurado como router.
enable_voip_rules_forward = False

Cuantas llamadas puede iniciar hacer un mismo host en una unidad de tiempo por defecto 12 por minuto.
voip_irate = '12/minute'

Cuantas registros puede iniciar hacer un mismo host en una unidad de tiempo por defecto 12 por minuto. En general los ataques intentan por fuerza bruta realizar registros de usuarios en el sistema voip esto limita estos registros baneando al host que quiera encontrar un usuario y un password válido.
voip_rrate = '10/minute'

Limita la cantidad de paquetes opcion proveniente de un mismo host a lo indicado  debajo
voip_orate = '40/minute'

Establece el tiempo que un host permanece bloqueado si se sobrepasan los limites antes descriptos. El tiempo se mide en milisegundos. Por defecto 120000 milisegundos = 120 segundos.
voip_hash_live_time = 120000

Ráfaga aceptada de paquetes antes de aplicar los limites antes descriptos
voip_burst = '15'

Si True se bloquean los paquetes que contentan el string tel.
voip_block_tel = True

Protocolo de compunicación que usa el sistema voip.
voip_protocol = 'udp'

Puerto de señailacion sip voip
voip_signal_port = 5060

Rango de puertos por el que se reciben los canales de audio.
voip_audio_range_port = '10000:20000'

Ofset a partir del cual se comenzaran a analizar los paquetes por el modulo string de iptables.
voip_offset = 65

Lista de servidores que no pasan por el chequeo de las reglas de la seccion voip, colocar aqui la lista de carriers de llamadas telefónicas.
voip_no_limit_servers = ['208.89.104.81']


### Bloqueo de redes por reputación.

Gracias al equipo de: https://rules.emergingthreats.net/ si esta en true se habilita el bloqueo de esta lista de reputación.
El parámetro que comienza por url_ indica la url de donde se descargara la lista de reputación.
enable_emergingthreats = True
url_emergingthreats = 'https://rules.emergingthreats.net/fwrules/emerging-Block-IPs.txt'


Gracias al equipo de: https://www.talosintelligence.com/reputation_center si esta en true se habilita el bloqueo de esta lista de reputación.
El parametro que comienza por url_ indica la url de donde se descargara la lista de reputación.
enable_talosintelligence = True
url_talosintelligence = "https://www.talosintelligence.com/documents/ip-blacklist"


Gracias al equipo de: http://iplists.firehol.org/ si esta en true se habilita el bloqueo de esta lista de reputación.
El parametro que comienza por url_ indica la url de donde se descargara la lista de reputación.
Dentro de una lan no se recomienda habilitar esta lista porque incluye la red 192.168.0.0/16 dentro de las redes a bloquear.
enable_firehol = False
url_firehol = 'https://iplists.firehol.org/files/firehol_level1.netset'



### Seteos generales

Carpeta donde se descargaran todas las zonas para su posterior procesamiento.
folder = './zones'  # download zone files into this folder

Url donde se descargan las listas de zonas de paises, Gracias al equipo de http://www.ipdeny.com
url_zones = 'http://www.ipdeny.com/ipblocks/data/countries/all-zones.tar.gz'  # Url to download the zones definitions

Si la opción de abajo se encuentra habilitada (True) las reglas o comandos que comiencen con: ipste, iptables, wget, iptables_save o iptables_restore seran reemplazadas por el comando el correspondiente string, útil para transformar comandos en la ruta absoluta del comando. Verificar que los comandos se encuetrán en la ruta mencionada para su distribución.

replace_executables = True

ipset = '/sbin/ipset'

iptables = '/sbin/iptables'

wget = '/usr/bin/wget'

iptables_save = '/sbin/iptables-save'

iptables_restore = '/sbin/iptables-restore'
