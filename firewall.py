#!/usr/bin/python3

"""
This program apply iptables and ipset rules to permit or deny acces to specific ports on specific net interface.
First: Config the file conf.py all variables you need and run firewall.py

Version: 1.0
This software have licence GPLv3
Creative common Attribution-ShareAlike 4.0 International: CC BY SA
"""

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def print_error(msg):
    print(bcolors.FAIL + msg + bcolors.ENDC)

def print_warning(msg):
    print(bcolors.WARNING + msg + bcolors.ENDC)

try:
    import sys
    import os
    from subprocess import Popen, PIPE, run
    import shlex
    import select
    import conf
    conf.excecute = True
    conf.verbose = False
    conf.debug = False
    conf.commands = {
    'ipset' : conf.ipset,
    'iptables' : conf.iptables,
    'wget' : conf.wget,
    'iptables-save' : conf.iptables_save,
    'iptables-restore' : conf.iptables_restore
    }
except Exception as ex:
    msg = 'Error: Not found all dependencies, install: subprocess, sys, os, shlex, select and create conf.py file'
    print_warning('-' * len(msg))
    print_error(msg)
    print_warning('-' * len(msg))
    print()
    print(ex)
    try:
        import traceback
        print(traceback.print_exc())
    except:
        print_error('Error: import traceback, this is a aditional error not the principal error, install python3 traceback module')
    # raise SystemExit(1)
    print_warning('-' * len(msg))
    print()
    raise SystemExit(1)


def pipecall(cmd, stdin_str):
    cmd_aux = shlex.split(cmd, comments=True, posix=True)
    if len(cmd_aux) > 0:
        if conf.replace_executables:
            command = cmd_aux[0]
            cmd_aux[0] = conf.commands.get(command, command)
            cmd = cmd.replace(command, conf.commands.get(command, command), 1)
        if conf.verbose:
            print('echo "%s" | %s' % (stdin_str, cmd))
        if conf.debug:
            print('processed command: %s' % str(cmd_aux))
        try:
            if conf.excecute:
                proc = Popen(cmd_aux, stdin=PIPE, stdout=PIPE, stderr=PIPE)
                r = proc.communicate(input=stdin_str.encode('utf8'))
                status  =  proc.returncode
                if status > 0:
                    print_error("Error executing: %s" % cmd)
                    print("Out:")
                    print(r[0])
                return status
            else:
                return 0
        except:
            print_error('Error Call: %s' % str(cmd))
            return 1
    else:
        return 0

def call(cmd, return_out = False, stdout=PIPE):
    cmd_aux = shlex.split(cmd, comments=True, posix=True)
    if len(cmd_aux) > 0:
        if conf.replace_executables:
            command = cmd_aux[0]
            cmd_aux[0] = conf.commands.get(command, command)
            cmd = cmd.replace(command, conf.commands.get(command, command), 1)
            # cmd = cmd.replace(cmd_aux[0], conf.commands[cmd_aux[0]], count=1)
        if conf.verbose:
            print(cmd)
        if conf.debug:
            print('processed command: %s' % str(cmd_aux))
        try:
            if conf.excecute:
                status = run(cmd_aux, stdout=stdout)
                if status.returncode > 0:
                    print_error("Error executing: %s" % cmd)
                if return_out:
                    return status.returncode, status.stdout.decode('utf-8')
                else:
                    return status.returncode
        except:
            print_error('Error Call: %s' % str(cmd))
            if return_out:
                return 1, ''
            else:
                return 1
    else:
        if return_out:
            return 0, ''
        else:
            return 0

def download_extract(silence=False):
    #download zones
    all_download_ok = True
    conf.folder = conf.folder if conf.folder[-1] == '/' else conf.folder + '/'
    extra_param = ' -q ' if silence else ''
    name_file = conf.url_zones.split('/')[-1]
    status = call('wget  %s -N   %s -O %s' % (extra_param, conf.url_zones, conf.folder + name_file))
    if status > 0:
        print_warning('Warning: Can\'t download Zones return signal: %i' % status)
        all_download_ok = False
    else:
        folder = conf.folder if conf.folder[-1] == '/' else conf.folder + '/'
        path = folder + name_file
        call('tar -xzf %s -C %s' % (path, folder))

    #download emergingthreats blocks
    name_file = conf.url_emergingthreats.split('/')[-1]
    status = call('wget  %s -N   %s -O %s' % (extra_param, conf.url_emergingthreats, conf.folder + name_file))
    if status > 0 and not silence:
        print_warning('Warning: Can\'t download emergingthreats blocks, return signal: %i' % status)
        all_download_ok = False

    #download talosintelligence blocks
    name_file = conf.url_talosintelligence.split('/')[-1]
    status = call('wget  %s -N   %s -O %s' % (extra_param, conf.url_talosintelligence, conf.folder + name_file))
    if status > 0 and not silence:
        print_warning('Warning: Can\'t download talosintelligence blocks, return signal: %i' % status)
        all_download_ok = False

    #download firehol blocks
    name_file = conf.url_firehol.split('/')[-1]
    status = call('wget  %s -N   %s -O %s' % (extra_param, conf.url_firehol, conf.folder + name_file))
    if status > 0 and not silence:
        print_warning('Warning: Can\'t download firehol blocks, return signal: %i' % status)
        all_download_ok = False

    if not silence and not all_download_ok:
        print_error('Error: Not all files are downloaded')
    return all_download_ok


def delete_rules():
    call('iptables -F')
    call('iptables -X')
    call('iptables -F -t nat')
    call('iptables -X -t nat')
    call('iptables -F -t mangle')
    call('iptables -X -t mangle')
    call('iptables -P INPUT ACCEPT')
    call('iptables -P OUTPUT ACCEPT')
    call('iptables -P FORWARD ACCEPT')
    call('ipset destroy')


def create_rules(excecute=True, verbose=False):
    #Create general zones
    ipset_rules = ''
    call('ipset create geoblock hash:net')
    call('ipset create geoaccept hash:net')
    call('ipset create geovoipaccept hash:net')
    call('iptables -N drop')
    #Add nets zones are totally blocked
    ip_set = set() # For performance reasons create this set to eliminate redundant net's or ip
    folder = conf.folder if conf.folder[-1] == '/' else conf.folder + '/'
    for z in conf.zones_deny:
        path = folder + z +'.zone'
        with open(path, mode='r') as f:
            nets = f.readlines()
        for net in nets:
            ip_set.add(shlex.split(net, comments=True, posix=True)[0])

    # Add to block table the IP's / Net's in emergingthreats
    # Add to block table the IP's / Net's in talosintelligence
    # Add to block table the IP's / Net's in firehol
    reputation_sites = ((conf.enable_emergingthreats, conf.url_emergingthreats),
                        (conf.enable_talosintelligence, conf.url_talosintelligence),
                        (conf.enable_firehol, conf.url_firehol))
    for enable, url in reputation_sites:
        if enable:
            path = folder + url.split('/')[-1]
            with open(path, mode='r') as f:
                ip = f.readlines()
            for i in ip:
                ip_ = shlex.split(i, comments=True, posix=True)
                if len(ip_) > 0:
                    ip_set.add(ip_[0])
    # add all block net's or ip to geoblock
    for ip in ip_set:
        if len(ip) > 0:
            ipset_rules += '-! add geoblock %s\n' % ip

    if len(conf.zones_accept) == 0 and (len(conf.udp_port_zones_accept) > 0 or len(conf.tcp_port_zones_accept) > 0):
        print_warning('Warning: No zones in conf.zones_accept but have conf.udp_port_zones_accept or conf.tcp_port_zones_accept ports, no zone rule match.')
        print_error("All port set has zoned acepted are reject because not accept traphic from any zone")
    #Add nets to zones accepted
    for z in conf.zones_accept:
        path = folder + z +'.zone'
        with open(path, mode='r') as f:
            nets = f.readlines()
        for net in nets:
            n = net if net[-1] != '\n' else net[:-1]
            ipset_rules +=  '-! add geoaccept %s\n' % n
    #Permit loopback traphic
    call('iptables -A INPUT -i lo -j ACCEPT')
    call('iptables -A OUTPUT -o lo -j ACCEPT')
    # create rules from conf.pre_iptables
    for cmd in conf.pre_iptables.splitlines():
        call(cmd)
    # limit icmp packages
    call('iptables -A INPUT -i %s -p icmp -m limit --limit  %s --limit-burst 10 -j ACCEPT' % (conf.interface, conf.icmp_limit))
    # blocked nets
    call('iptables -A INPUT -i %s -m set --match-set geoblock src -j drop' % (conf.interface))
    call('iptables -A FORWARD -i %s -m set --match-set geoblock src -j drop' % (conf.interface))
    #zoned port are accepted
    if len(conf.zones_accept) > 0:
        for port in conf.udp_port_zones_accept:
            call('iptables -A INPUT -i %s -m set --match-set geoaccept src -p udp --dport %s -j ACCEPT' % (conf.interface, port))
            call('iptables -A FORWARD -i %s -m set --match-set geoaccept src -p udp --dport %s -j ACCEPT' % (conf.interface, port))
        for port in conf.udp_port_zones_accept:
            call('iptables -A INPUT -i %s -p udp --dport %s -j DROP' % (conf.interface, port))
            call('iptables -A FORWARD -i %s -p udp --dport %s -j DROP' % (conf.interface, port))
    if len(conf.zones_accept) > 0:
        for port in conf.tcp_port_zones_accept:
            call('iptables -A INPUT -i %s -m set --match-set geoaccept src -p tcp --dport %s -j ACCEPT' % (conf.interface, port))
            call('iptables -A FORWARD -i %s -m set --match-set geoaccept src -p tcp --dport %s -j ACCEPT' % (conf.interface, port))
        for port in conf.tcp_port_zones_accept:
            call('iptables -A INPUT -i %s -p tcp --dport %s -j DROP' % (conf.interface, port))
            call('iptables -A FORWARD -i %s -p tcp --dport %s -j DROP' % (conf.interface, port))
    #Add nets to voip zones accepted
    for i in conf.voip_no_limit_servers:
        if conf.enable_voip_rules_input:
            call('iptables -A INPUT -i %s -p %s --dport %s  -s %s -j ACCEPT' % (conf.interface, conf.voip_protocol, conf.voip_signal_port, i))
            call('iptables -A INPUT -i %s -p %s --dport %s  -s %s -j ACCEPT' % (conf.interface, conf.voip_protocol, conf.voip_audio_range_port, i))
            call('iptables -A INPUT -i %s -p %s --dport %s  -d %s -j ACCEPT' % (conf.interface, conf.voip_protocol, conf.voip_signal_port, i))
            call('iptables -A INPUT -i %s -p %s --dport %s  -d %s -j ACCEPT' % (conf.interface, conf.voip_protocol, conf.voip_audio_range_port, i))
        if conf.enable_voip_rules_forward:
            call('iptables -A FORWARD -i %s -p %s --dport %s  -s %s -j ACCEPT' % (conf.interface, conf.voip_protocol, conf.voip_signal_port, i))
            call('iptables -A FORWARD -i %s -p %s --dport %s  -s %s -j ACCEPT' % (conf.interface, conf.voip_protocol, conf.voip_audio_range_port, i))
            call('iptables -A FORWARD -i %s -p %s --dport %s  -d %s -j ACCEPT' % (conf.interface, conf.voip_protocol, conf.voip_signal_port, i))
            call('iptables -A FORWARD -i %s -p %s --dport %s  -d %s -j ACCEPT' % (conf.interface, conf.voip_protocol, conf.voip_audio_range_port, i))

        if conf.enable_voip_rules_input or conf.enable_voip_rules_forward:
            for z in conf.voip_zones_accept:
                path = folder + z +'.zone'
                with open(path, mode='r') as f:
                    nets = f.readlines()
                for net in nets:
                    n = net if net[-1] != '\n' else net[:-1]
                    ipset_rules += '-! add geovoipaccept %s\n' % n

        # set limits rates for voip packages
        if conf.enable_voip_rules_input:
            call('iptables -A INPUT -i %s -p %s --dport %s -m set --match-set geovoipaccept src -m string --string "INVITE sip:" --algo bm --to %s -m hashlimit --hashlimit-above %s --hashlimit-burst %s --hashlimit-htable-expire %s --hashlimit-mode srcip,dstport --hashlimit-name sip_i_limit -j drop' % (conf.interface, conf.voip_protocol, conf.voip_signal_port, conf.voip_offset, conf.voip_irate, conf.voip_burst, conf.voip_hash_live_time))
            call('iptables -A INPUT -i %s -p %s --dport %s -m set --match-set geovoipaccept src -m string --string "REGISTER sip:" --algo bm --to %s -m hashlimit --hashlimit-above %s --hashlimit-burst %s --hashlimit-htable-expire %s --hashlimit-mode srcip,dstport --hashlimit-name sip_r_limit -j drop' % (conf.interface, conf.voip_protocol, conf.voip_signal_port, conf.voip_offset, conf.voip_rrate, conf.voip_burst, conf.voip_hash_live_time))
            call('iptables -A INPUT -i %s -p %s --dport %s -m set --match-set geovoipaccept src -m hashlimit --hashlimit-above %s --hashlimit-burst %s --hashlimit-htable-expire %s --hashlimit-mode srcip,dstport --hashlimit-name sip_o_limit -j drop' % (conf.interface, conf.voip_protocol, conf.voip_signal_port, conf.voip_orate, conf.voip_burst, conf.voip_hash_live_time))
            call('iptables -A INPUT -i %s -p %s --dport %s -m set --match-set geovoipaccept src -j ACCEPT ' % (conf.interface, conf.voip_protocol, conf.voip_signal_port))
            call('iptables -A INPUT -i %s -p %s --dport %s -m set --match-set geovoipaccept dst -j ACCEPT ' % (conf.interface, conf.voip_protocol, conf.voip_signal_port))
            call('iptables -A INPUT -i %s -p %s --dport %s -j drop' % (conf.interface, conf.voip_protocol, conf.voip_signal_port))
            # call('iptables -A INPUT -i %s -p %s --dport %s -m set --match-set geovoipaccept src -j ACCEPT ' % (conf.interface, conf.voip_protocol, conf.voip_audio_range_port))
            # call('iptables -A INPUT -i %s -p %s --dport %s -m set --match-set geovoipaccept dst -j ACCEPT ' % (conf.interface, conf.voip_protocol, conf.voip_audio_range_port))
            call('iptables -A INPUT -i %s -p %s --dport %s -j ACCEPT' % (conf.interface, conf.voip_protocol, conf.voip_audio_range_port))
        if conf.enable_voip_rules_forward:
            call('iptables -A FORWARD -i %s -p %s --dport %s -m set --match-set geovoipaccept src -m string --string "INVITE sip:" --algo bm --to %s -m hashlimit --hashlimit-above %s --hashlimit-burst %s --hashlimit-htable-expire %s --hashlimit-mode srcip,dstport --hashlimit-name sip_i_limit -j drop' % (conf.interface, conf.voip_protocol, conf.voip_signal_port, conf.voip_offset, conf.voip_irate, conf.voip_burst, conf.voip_hash_live_time))
            call('iptables -A FORWARD -i %s -p %s --dport %s -m set --match-set geovoipaccept src -m string --string "REGISTER sip:" --algo bm --to %s -m hashlimit --hashlimit-above %s --hashlimit-burst %s --hashlimit-htable-expire %s --hashlimit-mode srcip,dstport --hashlimit-name sip_r_limit -j drop' % (conf.interface, conf.voip_protocol, conf.voip_signal_port, conf.voip_offset, conf.voip_rrate, conf.voip_burst, conf.voip_hash_live_time))
            call('iptables -A FORWARD -i %s -p %s --dport %s -m set --match-set geovoipaccept src -m hashlimit --hashlimit-above %s --hashlimit-burst %s --hashlimit-htable-expire %s --hashlimit-mode srcip,dstport --hashlimit-name sip_o_limit -j drop' % (conf.interface, conf.voip_protocol, conf.voip_signal_port, conf.voip_orate, conf.voip_burst, conf.voip_hash_live_time))
            call('iptables -A FORWARD -i %s -p %s --dport %s -m set --match-set geovoipaccept src -j ACCEPT ' % (conf.interface, conf.voip_protocol, conf.voip_signal_port))
            call('iptables -A FORWARD -i %s -p %s --dport %s -m set --match-set geovoipaccept dst -j ACCEPT ' % (conf.interface, conf.voip_protocol, conf.voip_signal_port))
            call('iptables -A FORWARD -i %s -p %s --dport %s -j drop' % (conf.interface, conf.voip_protocol, conf.voip_signal_port))
            # call('iptables -A FORWARD -i %s -p %s --dport %s -m set --match-set geovoipaccept src -j ACCEPT ' % (conf.interface, conf.voip_protocol, conf.voip_audio_range_port))
            # call('iptables -A FORWARD -i %s -p %s --dport %s -m set --match-set geovoipaccept dst -j ACCEPT ' % (conf.interface, conf.voip_protocol, conf.voip_audio_range_port))
            call('iptables -A FORWARD -i %s -p %s --dport %s -j ACCEPT' % (conf.interface, conf.voip_protocol, conf.voip_audio_range_port))

    #Other important rules
    call('iptables -A INPUT -m state -i %s --state RELATED,ESTABLISHED -j ACCEPT' % (conf.interface ))
    if conf.log:
        call('iptables -A drop -j LOG --log-prefix "ET BLOCK: "')
    call('iptables -A drop -j DROP')
    if conf.enable_udp_tcp_thresshold:
        call('iptables -A INPUT -p tcp -m state --state NEW -m hashlimit --hashlimit-above %s --hashlimit-burst %s --hashlimit-mode srcip,dstport --hashlimit-name tcp_limit -j drop' % (conf.udp_tcp_rate, conf.udp_tcp_burst))
        call('iptables -A INPUT -p udp -m state --state NEW -m hashlimit --hashlimit-above %s --hashlimit-burst %s --hashlimit-mode srcip,dstport --hashlimit-name udp_limit -j drop' % (conf.udp_tcp_rate, conf.udp_tcp_burst))
    # create rules from conf.post_iptables
    for cmd in conf.post_iptables.splitlines():
        call(cmd)
    if conf.at_end_add_input_forward_drop:
        call('iptables -A INPUT -j drop')
        call('iptables -A FORWARD -j drop')
    if pipecall('ipset restore', ipset_rules) > 0:
        print_error("Error call ipset rules")

def save_rules(verbose=False):
    if call('ipset save -f /tmp/ipset_test_rules_firewall') != 0:
        print_error('Can\'t save ipset rules, exit.')
        sys.exit(1)
    ipt_rules = call('iptables-save -c', return_out = True)
    if ipt_rules[0] == 0:
        with open('/tmp/iptables_test_rules_firewall', mode='w') as f:
            nets = f.write(ipt_rules[1])
    else:
        print_error('Can\'t save iptables rules, exit.')
        sys.exit(1)
    if verbose:
        print("Rules saves in files: /tmp/ipset_test_rules_firewall and /tmp/iptables_test_rules_firewall")

def restore_rules():
    call('ipset restore -f /tmp/ipset_test_rules_firewall')
    call('iptables-restore -c /tmp/iptables_test_rules_firewall')
    call('rm /tmp/ipset_test_rules_firewall')
    call('rm /tmp/iptables_test_rules_firewall')

def print_help():
    print("""
    This is an firewall assistence, its developed by Daniel Zipitría to resolve a particular solutión of firewall but are developed
    with the policy of being as general as possible, simply personalice the file config.py and run ./firewall.py <action> to perform any of this actions:
    For this reason this firewall assistance at this time are used in many heterogeneous situations.

    start: Apply the iptables rules and ipset filters, not remove the excist iptable rules.

    stop: Remove all iptables rules and ipset filters.

    restart: Perform a stop action and start action.

    test: Recomended action.
          Save all rules and ipset filters, perform a stop and start action and ask for some seconds if you like to mantain this rules and filters.
          If you answer N or n or wait for many seconds the iptables and filter are revert to original state.

    simulate: Not do anything only print all rules and filters are applied if you use the start action.

    status: Print all iptables rules.

    status-ipset: Print all ipset filters.

    download: Download from internet all blacklist defined in config.py printing the process.

    silent_download: As download action but not print the progress status

    save: Save the iptables filters and ipset rules in /tmp/ directory

    help: Print this help ;-)

    """)

if __name__ == '__main__':
    try:
        cmd = sys.argv[1]
    except:
        print("Usage %s, [start|stop|restart|test|simulate|status|status-ipset|download|silent_download|save]" % (sys.argv[0]))
        sys.exit(1)
    cmd = cmd.lower()
    download = True
    if not os.path.isdir(conf.folder):
        print("Warning: Not exisist %s folder, creating this..." % (conf.folder))
        os.makedirs(conf.folder)
        print_warning("Warning: Downloading zones files...")
        if download_extract():
            download = False
        else:
            sys.excit(1)
    if cmd == 'start':
        create_rules()
        sys.exit(0)
    elif cmd == 'stop':
        delete_rules()
        sys.exit(0)
    elif cmd == 'restart':
        delete_rules()
        create_rules()
        sys.exit(0)
    elif cmd == 'test':
        save_rules()
        delete_rules()
        create_rules()
        print("Keep the rules? if don't answer remove in 50 secconds(y/n):", end='', flush=True)
        i, _, _ = select.select( [sys.stdin], [], [], 50 )
        if i:
            response = sys.stdin.readline().strip()
            if response.lower() != 'y':
                delete_rules()
                restore_rules()
        else:
            delete_rules()
            restore_rules()
        sys.exit(0)
    elif cmd == 'simulate':
        conf.excecute = False
        conf.verbose = True
        create_rules()
        sys.exit(0)
    elif cmd == 'status':
        call('iptables -L -v -n', stdout=None)
        sys.exit(0)
    elif cmd == 'status-ipset':
        call('ipset list', stdout=None)
        sys.exit(0)
    elif cmd == 'download' or cmd == 'silent_download':
        if download:
            sys.exit(int(not download_extract(silence = cmd != 'download')))
    elif cmd == 'save':
        save_rules(verbose=True)
    elif cmd == 'help':
        print_help()
    else:
        print("Usage %s, [start|stop|reload|test|simulate|status|status-ipset|download|silent_download|save|help]" % (sys.argv[0]))
        sys.exit(1)
